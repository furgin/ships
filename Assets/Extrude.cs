using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IExtrude
{
    IEnumerable<Face> Apply(Face face);
}

public class Extrude : IExtrude
{
    private float Distance { get; }
    private Vector2 Scale { get; }
    public Vector2 Offset { get; }
    private Vector2 Translate { get; }
    public IEnumerable<ChildExtrusionChain> Children { get; }

    public string[] Tags { get; }

    private Extrude(string[] tags,
        float distance,
        Vector2 scale,
        Vector2 offset,
        Vector2 translate,
        IEnumerable<ChildExtrusionChain> children)
    {
        Distance = distance;
        Scale = scale;
        Offset = offset;
        Translate = translate;
        Children = children;
        Tags = tags;
    }

    private void ExtrudeFace(Face face)
    {
        var oldPos = (face.Vertices.Aggregate((a, b) => a + b) / face.Vertices.Length);

        var axisA = face.AxisA;
        var axisB = face.AxisB;

        face.Vertices = face.Vertices.Select(v =>
            {
                var move = v + face.LocalUp * Distance;
                var pv = (v - oldPos);

                var a = Vector3.Project(pv, axisA);
                var b = Vector3.Project(pv, axisB);

                var scale = (a * (Scale.y - 1f) + b * (Scale.x - 1f));
                var offset = (a.normalized * Offset.y + b.normalized * Offset.x);
                var translate = (axisA.normalized * Translate.y + axisB.normalized * Translate.x);

                return move + translate + offset + scale;
            }
        ).ToArray();
    }

    public IEnumerable<Face> Apply(Face face)
    {
        var axisA = new Vector3(face.LocalUp.y, face.LocalUp.z, face.LocalUp.x);
        var axisB = Vector3.Cross(face.LocalUp, axisA);

        var oldVertices = new[]
        {
            new Vector3(face.Vertices[0].x, face.Vertices[0].y, face.Vertices[0].z),
            new Vector3(face.Vertices[1].x, face.Vertices[1].y, face.Vertices[1].z),
            new Vector3(face.Vertices[2].x, face.Vertices[2].y, face.Vertices[2].z),
            new Vector3(face.Vertices[3].x, face.Vertices[3].y, face.Vertices[3].z)
        };

        var faces = new List<Face>();
        ExtrudeFace(face);

        // 4 faces of extrusion
        faces.Add(new Face(
            Tags,
            Vector3.Cross(face.Vertices[3] - oldVertices[2], face.Vertices[2] - oldVertices[3]).normalized,
            new[] {face.Vertices[3], face.Vertices[2], oldVertices[2], oldVertices[3]}));
        faces.Add(new Face(
            Tags,
            Vector3.Cross(face.Vertices[1] - oldVertices[0], face.Vertices[0] - oldVertices[1]).normalized,
            new[] {face.Vertices[1], face.Vertices[0], oldVertices[0], oldVertices[1]}));
        faces.Add(new Face(
            Tags,
            Vector3.Cross(face.Vertices[2] - oldVertices[1], face.Vertices[1] - oldVertices[2]).normalized,
            new[] {face.Vertices[2], face.Vertices[1], oldVertices[1], oldVertices[2]}));
        faces.Add(new Face(
            Tags,
            Vector3.Cross(face.Vertices[0] - oldVertices[3], face.Vertices[3] - oldVertices[0]).normalized,
            new[] {face.Vertices[0], face.Vertices[3], oldVertices[3], oldVertices[0]}));

        // set mirrors if this is a segment
        if (HasTag("segment"))
        {
            faces[0].Mirror = faces[1];
            faces[1].Mirror = faces[0];
            faces[2].Mirror = faces[3];
            faces[3].Mirror = faces[2];
        }

        // tag cardinal directions
        string[] names = {"up", "down", "forward", "back", "left", "right"};
        Vector3[] directions = {Vector3.up, Vector3.down, Vector3.forward, Vector3.back, Vector3.left, Vector3.right};

        foreach (var f in faces)
        {
            for (var index = 0; index < directions.Length; index++)
            {
                var direction = directions[index];
                if ((f.Direction - direction).magnitude < Mathf.Epsilon)
                {
                    f.AddTag(names[index]);
                }
            }
        }

        // execute children extrusions
        foreach (var child in Children)
        {
            var find = faces.Find(f => f.HasTag(child.Tag));
            if (find != null)
            {
                faces.AddRange(child.Child.Apply(find));
            }
        }

        return faces;
    }

    public static ExtrudeBuilder Builder()
    {
        return new ExtrudeBuilder();
    }

    public class ExtrudeBuilder
    {
        private float _distance = 0.0f;
        private Vector2 _scale = new Vector2(1f, 1f);
        private Vector2 _offset = Vector2.zero;
        private Vector2 _translate = Vector2.zero;
        private readonly List<string> _tags = new List<string>();
        private readonly List<ChildExtrusionChain> _children = new List<ChildExtrusionChain>();

        public ExtrudeBuilder Distance(float distance)
        {
            _distance = distance;
            return this;
        }

        public ExtrudeBuilder Tag(string tag)
        {
            _tags.Add(tag);
            return this;
        }

        public ExtrudeBuilder Scale(float scaleX, float scaleY)
        {
            _scale = new Vector2(scaleX, scaleY);
            return this;
        }

        public ExtrudeBuilder Scale(float scale)
        {
            _scale = new Vector2(scale, scale);
            return this;
        }

        public ExtrudeBuilder Scale(Vector2 scale)
        {
            _scale = scale;
            return this;
        }

        public ExtrudeBuilder Offset(Vector2 offset)
        {
            _offset = offset;
            return this;
        }

        public ExtrudeBuilder Offset(float x, float y)
        {
            _offset = new Vector2(x, y);
            return this;
        }

        public ExtrudeBuilder Offset(float offset)
        {
            _offset = new Vector2(offset, offset);
            return this;
        }

        public Extrude Build()
        {
            return new Extrude(_tags.ToArray(), _distance, _scale, _offset, _translate, _children);
        }

        public ExtrudeBuilder Translate(Vector2 translate)
        {
            _translate = translate;
            return this;
        }

        public ExtrudeBuilder Translate(float translate)
        {
            _translate = new Vector2(translate, translate);
            return this;
        }

        public ExtrudeBuilder Translate(float x, float y)
        {
            _translate = new Vector2(x, y);
            return this;
        }


        public ExtrudeBuilder TranslateX(float x)
        {
            _translate = new Vector2(x, _translate.y);
            return this;
        }

        public ExtrudeBuilder TranslateY(float y)
        {
            _translate = new Vector2(_translate.x, y);
            return this;
        }

        public ExtrudeBuilder OffsetX(float x)
        {
            _offset = new Vector2(x, _offset.y);
            return this;
        }

        public ExtrudeBuilder OffsetY(float y)
        {
            _offset = new Vector2(_offset.x, y);
            return this;
        }

        public ExtrudeBuilder Extrude(string tag, IExtrude child)
        {
            if (child != null)
            {
                _children.Add(new ChildExtrusionChain(tag, child));
            }

            return this;
        }

        public ExtrudeBuilder ScaleX(float x)
        {
            _scale = new Vector2(x, _scale.y);
            return this;
        }

        public ExtrudeBuilder ScaleY(float y)
        {
            _scale = new Vector2(_scale.x, y);
            return this;
        }
    }

    public bool HasTag(string tag)
    {
        return Tags.Contains(tag);
    }
}