﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class CreateMesh : MonoBehaviour
{
    private List<Face> _faces;
    public int Seed = 1;
    public Text SeedText;
    private Vector3 _offset;

    private void CreateShip()
    {
        var random = new RandomSequence(Seed);
        _faces = new List<Face>();
        _faces.AddRange(CreateCube(random.IntValue()));
        _faces.AddRange(BuildLibrary.ForwardChain(random.IntValue()).Apply(_faces.Find(f => f.HasTag("forward"))));
        _faces.AddRange(BuildLibrary.BackChain(random.IntValue()).Apply(_faces.Find(f => f.HasTag("back"))));
    }

    private void OnValidate()
    {
        CreateShip();
        GenerateMesh();
    }

    private void GenerateMesh()
    {
        var combine = new CombineInstance[_faces.Count];

        for (var i = 0; i < _faces.Count; i++)
        {
            _faces[i].ConstructMesh();
            combine[i] = new CombineInstance
            {
                mesh = _faces[i].ConstructMesh(),
                transform = Matrix4x4.identity
            };
        }

        var mesh = new Mesh();
        mesh.CombineMeshes(combine);
        mesh.RecalculateBounds();
        var bounds = mesh.bounds;
        var center = bounds.center;
        var vertices = mesh.vertices;
        for (var index = 0; index < vertices.Length; index++)
        {
            vertices[index] -= center;
        }

        mesh.SetVertices(vertices);
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        _offset = center;

        var meshFilter = GetComponent<MeshFilter>();
        meshFilter.sharedMesh = mesh;
    }

    private static IEnumerable<Face> CreateCube(int seed)
    {
        var hashFunction = new MurmurHash(seed);

        string[] names = {"up", "down", "forward", "back", "left", "right"};
        Vector3[] directions = {Vector3.up, Vector3.down, Vector3.forward, Vector3.back, Vector3.left, Vector3.right};
        int[] mirror = {1, 0, 3, 2, 5, 4};

        var sizeX = hashFunction.Range(0.8f, 1.5f, 0);
        var sizeY = hashFunction.Range(0.8f, 1.1f, 0);
        var sizeZ = hashFunction.Range(0.8f, 1.1f, 0);
        var faces = names
            .Select((t, i) => Face.Square(new[] {t}, directions[i], new Vector3(sizeX, sizeY, sizeZ)))
            .ToList();

        for (var i = 0; i < faces.Count; i++)
        {
            faces[i].Mirror = faces[mirror[i]];
        }

        return faces;
    }

    private void Start()
    {
        Generate();
    }

    private void OnDrawGizmos()
    {
        var position = transform.position;
        foreach (var face in _faces)
        {
            Gizmos.color = Color.blue;
            var center = face.Center;
            Gizmos.DrawLine(position + center - _offset, position + center + face.LocalUp - _offset);
            Gizmos.color = Color.green;
            Gizmos.DrawLine(position + center - _offset, position + center + face.Direction * 0.5f - _offset);
            if (face.Mirror != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(position + center - _offset, position + face.Mirror.Center - _offset);
            }
        }
    }

    private void Generate()
    {
        Seed = Random.Range(0, int.MaxValue);
        CreateShip();
        GenerateMesh();
        SeedText.text = Seed.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Generate();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}