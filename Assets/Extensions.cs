using System;
using UnityEngine;

public static class Extensions
{
    public static Vector3 Direction(this Vector3 v)
    {
        var vv = new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        if (vv.x > vv.y && vv.x > vv.z)
        {
            return new Vector3(Mathf.Sign(v.x), 0f, 0f);
        }
        if (vv.y > vv.x && vv.y > vv.z)
        {
            return new Vector3(0,Mathf.Sign(v.y), 0f);
        }
        if (vv.z > vv.x && vv.z > vv.y)
        {
            return new Vector3(0,0, Mathf.Sign(v.y));
        }
        return Vector3.zero;
    }
}