using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Face
{
    public Vector3 Center
    {
        get { return (Vertices.Aggregate((a, b) => a + b) / Vertices.Length); }
    }

    public Vector3 LocalUp { get; }
    public Vector3 Direction => LocalUp.Direction();
    public Vector3[] Vertices { get; internal set; }
    private string[] _tags;
    public Face Mirror { get; set; }
    public Vector3 AxisA => GetLocalAxis(this.LocalUp)[0];
    public Vector3 AxisB => GetLocalAxis(this.LocalUp)[1];

    public static Vector3[] GetLocalAxis(Vector3 localUp)
    {
        var axisA = Vector3.zero;
        var axisB = Vector3.zero;
        var localUpN = localUp.normalized;
        if (Math.Abs(Vector3.Dot(Vector3.forward, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.up;
            axisB = Vector3.left;
        }
        else if (Math.Abs(Vector3.Dot(Vector3.back, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.up;
            axisB = Vector3.left;
        }
        else if (Math.Abs(Vector3.Dot(Vector3.left, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.up;
            axisB = Vector3.forward;
        }
        else if (Math.Abs(Vector3.Dot(Vector3.right, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.up;
            axisB = Vector3.forward;
        }
        else if (Math.Abs(Vector3.Dot(Vector3.up, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.left;
            axisB = Vector3.forward;
        }
        else if (Math.Abs(Vector3.Dot(Vector3.down, localUpN) - 1f) < Mathf.Epsilon)
        {
            axisA = Vector3.left;
            axisB = Vector3.forward;
        }
        else
        {
            Debug.Log("unknown: " + localUp);
        }

        return new[] {axisA, axisB};
    }

    public static bool IsReverse(Vector3 localUp)
    {
        var localUpN = localUp.normalized;
        if (Math.Abs(Vector3.Dot(Vector3.back, localUpN) - 1f) < Mathf.Epsilon)
        {
            return true;
        }
        if (Math.Abs(Vector3.Dot(Vector3.left, localUpN) - 1f) < Mathf.Epsilon)
        {
            return true;
        }
        if (Math.Abs(Vector3.Dot(Vector3.down, localUpN) - 1f) < Mathf.Epsilon)
        {
            return true;
        }

        Debug.Log("unknown: " + localUp);
        return false;
    }

    public static Face Square(string[] tags, Vector3 localUp, Vector3 size)
    {
        var axis = GetLocalAxis(localUp);
        var axisA = axis[0];
        var axisB = axis[1];

        Vector3[] vertices =
        {
            Vector3.Scale(size, (axisA * 0.5f) - (axisB * 0.5f) + localUp * .5f),
            Vector3.Scale(size, (axisA * 0.5f) + (axisB * 0.5f) + localUp * .5f),
            Vector3.Scale(size, -(axisA * 0.5f) + (axisB * 0.5f) + localUp * .5f),
            Vector3.Scale(size, -(axisA * 0.5f) - (axisB * 0.5f) + localUp * .5f)
        };

        if (IsReverse(localUp))
        {
            vertices = vertices.Reverse().ToArray();
        }

        return new Face(tags, localUp, vertices);
    }

    public Face(string[] tags, Vector3 localUp, Vector3[] vertices)
    {
        _tags = tags;
        LocalUp = localUp;
        Vertices = vertices;
    }

    public Mesh ConstructMesh()
    {
        var mesh = new Mesh();
        int[] triangles =
        {
            1, 2, 0,
            2, 3, 0
        };

        mesh.Clear();
        mesh.vertices = Vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        return mesh;
    }


    public void AddTag(string tag)
    {
        _tags = _tags.Concat(new[] {tag}).ToArray();
    }

    public bool HasTag(string tag)
    {
        return _tags.Contains(tag);
    }
}