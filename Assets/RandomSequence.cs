public class RandomSequence
{
    private int _number;
    private readonly HashFunction _hashFunction;

    public RandomSequence(int seed)
    {
        _number = 0;
        _hashFunction = new MurmurHash(seed);
    }

    public int Range(int min, int max)
    {
        return _hashFunction.Range(min, max, new []{_number++});
    }

    public float Range(float min, float max)
    {
        return _hashFunction.Range(min, max, new []{_number++});
    }

    public float FloatValue()
    {
        return _hashFunction.Value(new []{_number++});
    }

    public int IntValue()
    {
        return _hashFunction.IntValue(new []{_number++});
    }
}