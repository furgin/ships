using System.Collections.Generic;
using System.Linq;

public class ChildExtrusionChain
{
    public string Tag { get; }
    public IExtrude Child { get; }

    public ChildExtrusionChain(string tag, IExtrude child)
    {
        Tag = tag;
        Child = child;
    }
}

public class ExtrusionChain : IExtrude
{
    public IExtrude[] Extrusions { get; }

    public ExtrusionChain(IExtrude[] extrusions)
    {
        Extrusions = extrusions;
    }

    public static ExtrusionChainBuilder Builder()
    {
        return new ExtrusionChainBuilder();
    }

    public class ExtrusionChainBuilder
    {
        private readonly List<IExtrude> _extrusions = new List<IExtrude>();

        public ExtrusionChainBuilder Add(IExtrude child)
        {
            if (child != null)
            {
                _extrusions.Add(child);
            }
            return this;
        }

        public ExtrusionChain Build()
        {
            return new ExtrusionChain(_extrusions.ToArray());
        }
    }

    public IEnumerable<Face> Apply(Face face)
    {
        var faces = new List<Face>();

        foreach (var extrusion in Extrusions)
        {
            var newFaces = extrusion.Apply(face);
            faces.AddRange(newFaces.ToArray());
        }

        faces.Add(face);

        return faces;
    }
}