using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BuildLibrary
{
    private static IEnumerable<Face> BuildOutSegment(Face face, int seed)
    {
        var hashFunction = new MurmurHash(seed);
        var chain = new ExtrusionChain.ExtrusionChainBuilder();

        if (hashFunction.Value(0) > 0.75f)
        {
            var distance = hashFunction.Range(0.2f, 0.8f, 1);
            var offsetX = hashFunction.Range(-0.1f, 0f, 2);
            var offsetY = hashFunction.Range(-0.1f, 0f, 3);

            chain.Add(new Extrude.ExtrudeBuilder().Distance(distance).Offset(offsetX, offsetY).Build());
        }

        return chain.Build().Apply(face).ToArray();
    }


    public static IExtrude CommandSpire(int seed)
    {
        var random = new RandomSequence(seed);
        var chain = ExtrusionChain.Builder()
                .Add(Extrude.Builder().Distance(random.Range(0.1f, 0.3f)).Build())
                .Add(Extrude.Builder()
                    .Distance(random.Range(0.1f, 0.3f))
                    .Scale(random.Range(0.8f, 0.9f))
                    .Build())
                .Add(Extrude.Builder().Distance(random.Range(0.1f, 0.3f)).Build())
                .Add(Extrude.Builder().OffsetY(random.Range(0.2f, 0.8f)).Distance(random.Range(0.05f, 0.15f)).Build())
                .Add(Extrude.Builder().Distance(random.Range(0.05f, 0.15f)).Build())
            ;
        return chain.Build();
    }

    public static IExtrude Bevel(int seed)
    {
        var random = new RandomSequence(seed);
        var bevelBuilder = ExtrusionChain.Builder()
            .Add(Extrude.Builder().Distance(random.Range(0.1f, 0.2f)).Scale(random.Range(0.8f, 0.95f)).Build())
            .Add(Extrude.Builder().Distance(0.1f).Scale(0.9f).Build());
        return bevelBuilder.Build();
    }

    public static IExtrude Indent(int seed)
    {
        var indentBuilder = ExtrusionChain.Builder()
            .Add(Extrude.Builder().Scale(0.9f).Build())
            .Add(Extrude.Builder().Distance(-0.1f).Build());
        return indentBuilder.Build();
    }

    public static IExtrude Grill(int seed)
    {
        var random = new RandomSequence(seed);
        var num = random.Range(3, 5);
        var distance = random.Range(0.05f, 0.15f);
        var scale = random.Range(0.9f, 0.9f);

        var builder = ExtrusionChain.Builder();

        for (var j = 0; j < num; j++)
        {
            builder
                .Add(Extrude.Builder().Scale(scale).Build())
                .Add(Extrude.Builder().Distance(distance).Build())
                .Add(Extrude.Builder().Scale(1f / scale).Build())
                .Add(Extrude.Builder().Distance(distance).Build());
        }

        return builder.Build();
    }

    public static IExtrude Wing(int seed)
    {
        var random = new RandomSequence(seed);
        var wingBuilder = ExtrusionChain.Builder();

        // create wings
        var wingRandomSelect = new ActionRandomSelect(4);
        wingRandomSelect.Add(2, () =>
        {
            wingBuilder
                .Add(Extrude.Builder()
                    .Distance(random.Range(0.2f, 0.4f))
                    .Scale(random.Range(0.5f, 0.9f))
                    .Build())
                .Add(Extrude.Builder()
                    .Distance(random.Range(0.2f, 0.3f))
                    .Scale(new Vector2(1f, random.Range(0.03f, 0.5f)))
//                    .Translate(random.Range(0.0f, 0.5f), 0f)
                    .Build())
                ;
        });
        wingRandomSelect.Add(1, () =>
        {
            wingBuilder.Add(Extrude.Builder()
                .Distance(random.Range(0.5f, 0.9f))
                .Scale(0.9f)
                .Build());
        });
        wingRandomSelect.Choose(random.FloatValue()).Invoke();

        return wingBuilder.Build();
    }

    public static IExtrude RandomDetail(int seed)
    {
        var random = new RandomSequence(seed);
        var detailRandomSelect = new FunctionRandomSelect<IExtrude>(10);
        detailRandomSelect.Add(5, () => Indent(random.IntValue()));
        detailRandomSelect.Add(5, () => Bevel(random.IntValue()));
        return detailRandomSelect.Choose(random.FloatValue())?.Invoke();
    }

    public static IExtrude Segment(int seed)
    {
        var random = new RandomSequence(seed);

        var shape = ExtrusionChain.Builder()
            .Add(Extrude.Builder().Distance(random.Range(0.1f, 0.2f)).Build())
            .Add(Extrude.Builder().Distance(random.Range(0.1f, 0.2f)).Offset(0, random.Range(0.1f, 0.2f)).Build())
            .Build();

        // simple extrude
        var builder = Extrude.Builder()
            .Tag("segment")
            .Extrude("left", shape)
            .Extrude("right", shape)
            .Extrude("up", shape)
            .Extrude("down", shape)
            .Distance(random.Range(0.5f, 2f));

        var randomSelect = new ActionRandomSelect();
        randomSelect.Add(2, () =>
        {
            // wings
            var indent = Wing(random.IntValue());
            builder
                .Extrude("left", indent)
                .Extrude("right", indent);

            if (random.FloatValue() > 0.5f)
            {
                builder.Extrude("up", indent)
                    .Extrude("down", indent);
            }
        });
        randomSelect.Add(1, () =>
        {
            // small indent
            var grill = Grill(random.IntValue());
            builder
                .Extrude("left", grill)
                .Extrude("right", grill)
                .Extrude("left", Bevel(random.IntValue()))
                .Extrude("right", Bevel(random.IntValue()));
        });

        var randomDetail = RandomDetail(random.IntValue());
        builder
            .Extrude("left", randomDetail)
            .Extrude("right", randomDetail)
            .Extrude("up", randomDetail)
            .Extrude("down", randomDetail);

        randomSelect.Choose(random.FloatValue()).Invoke();

        return builder.Build();
    }

    public static IExtrude BackChain(int seed)
    {
        var random = new RandomSequence(seed);
        var numSegments = random.Range(2, 3);

        var chain = ExtrusionChain.Builder();

        var randomSelect = new ActionRandomSelect();
        randomSelect.Add(5, () => { chain.Add(Segment(random.IntValue())); });

        for (var i = 0; i < numSegments; i++)
        {
            randomSelect.Choose(random.FloatValue()).Invoke();
            if(i<=numSegments-1) { chain.Add(Extrude.Builder().ScaleX(1.2f).Build());}
        }

        return chain.Build();
    }
    
    public static IExtrude ForwardChain(int seed)
    {
        var random = new RandomSequence(seed);
        var numSegments = random.Range(10, 20);

        var chain = ExtrusionChain.Builder();

        var randomSelect = new ActionRandomSelect();
        randomSelect.Add(5, () => { chain.Add(Segment(random.IntValue())); });
        randomSelect.Add(1, () =>
        {
            // extrusion with scale
            var distance = random.Range(0.1f, 0.5f);
            var scale = random.Range(0.7f, 1.3f);
            chain.Add(Extrude.Builder().Distance(distance).Scale(scale).Build());
        });
        randomSelect.Add(20, () =>
        {
            // simple offset
            var distance = random.Range(0.1f, 0.5f);
            var offsetY = random.Range(-0.1f, 0.1f);
            var offsetX = random.Range(-0.1f, 0.1f);
            chain.Add(Extrude.Builder().Distance(distance).Offset(offsetX, offsetY).Build());
        });
        randomSelect.Add(1, () =>
        {
            // simple translate
            var distance = random.Range(0.1f, 0.5f);
            var translateX = random.Range(-0.1f, 0.1f);
            var translateY = random.Range(-0.1f, 0.1f);
            chain.Add(Extrude.Builder().Distance(distance).Translate(translateX, translateY).Build());
        });
        randomSelect.Add(1, () => { chain.Add(Grill(random.IntValue())); });

        for (var i = 0; i < numSegments; i++)
        {
            randomSelect.Choose(random.FloatValue()).Invoke();
        }

        return chain.Build();
    }
}