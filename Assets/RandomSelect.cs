using System;
using System.Collections.Generic;
using System.Linq;

internal class Option<T>
{
    internal int Calls = 0;
    internal int Max { get; }
    public float Weight { get; }
    public T Action { get; }

    public Option(float weight, int max, T action)
    {
        Max = max;
        Weight = weight;
        Action = action;
    }
}

public class RandomSelect<T>
{
    private readonly float _nothingWeight;
    private readonly T _nothing;
    private readonly List<Option<T>> _options = new List<Option<T>>();

    public RandomSelect(float nothingWeight, T nothing)
    {
        _nothingWeight = nothingWeight;
        _nothing = nothing;
    }

    public void Add(float weight, T option, int max = 0)
    {
        _options.Add(new Option<T>(weight, max, option));
    }

    public T Choose(float value)
    {
        var acc = 0f;
        var totalWeight = _nothingWeight +
                          _options
                              .Where((o) => o.Max==0 || o.Calls < o.Max)
                              .Select(o => o.Weight).Sum();
        foreach (var option in _options.Where((o) => o.Max==0 || o.Calls < o.Max))
        {
            var realWeight = option.Weight / totalWeight;
            acc += realWeight;
            if (value < acc)
            {
                option.Calls++;
                return option.Action;
            }
        }

        return _nothing;
    }
}

public class ActionRandomSelect : RandomSelect<Action>
{
    public ActionRandomSelect(float nothingWeight = 0) : base(nothingWeight, () => { })
    {
    }
}

public class FunctionRandomSelect<T> : RandomSelect<Func<T>>
{
    public FunctionRandomSelect(float nothingWeight = 0) : base(nothingWeight, null)
    {
    }
}