# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.22

- patch: fixed axis alignment
- patch: major refactor
- patch: more ship like details
- patch: stacking of random effects including ship shape

## 0.0.21

- patch: $*
- patch: $@

## 0.0.20

- patch: builds wings

## 0.0.19

- patch: Release script testing

## 0.0.18

- patch: Release script testing

## 0.0.17

- patch: deployment script

## 0.0.16

- patch: Release script testing

## 0.0.15

- patch: Release script testing

## 0.0.14

- patch: Release script testing

## 0.0.13

- patch: Release script testing
- patch: Release script testing

## 0.0.12

- patch: Release script testing

## 0.0.11

- patch: Release script testing

## 0.0.10

- patch: Release script testing

## 0.0.9

- patch: Refactoring
- patch: Refactoring
- patch: Release script testing

## 0.0.8

- patch: Test release branch trigger again

## 0.0.7

- patch: Add version text

## 0.0.6

- patch: Test release branch trigger again

## 0.0.5

- patch: Test release branch trigger again

## 0.0.4

- patch: Test release branch trigger again

## 0.0.3

- patch: Test release branch trigger again

## 0.0.2

- patch: test release branch

## 0.0.1

- patch: Center mesh correctly
- patch: Random starting cube size

