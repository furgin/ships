#!/usr/bin/env bash

host="megatron.furgin.org"
dir=$(pwd)
branch=$(git rev-parse --abbrev-ref HEAD)
if [ "$branch" == "master" ];
then
    version=$(git rev-parse --short HEAD)
else
    version=$(semversioner current-version)
fi

mkdir "${dir}/Deploy"

if [[ -d Builds/StandaloneOSX ]]; then
    cd Builds/StandaloneOSX && genisoimage -l -R -J -V "Ships" -D -apple -no-pad -o "${dir}/Deploy/ships-${version}.dmg" . && cd ${dir}
fi 

if [[ -d Builds/StandaloneWindows64 ]]; then
    cd Builds/StandaloneWindows64 && zip -r "${dir}/Deploy/ships-${version}.zip" . && cd ${dir}
fi 

if [[ -d Builds/StandaloneLinux64 ]]; then
    cd Builds/StandaloneLinux64 && tar cvzf "${dir}/Deploy/ships-${version}.tar.gz" . && cd ${dir}
fi 

if [[ -d Builds/WebGL ]]; then
    cd Builds/WebGL && zip -r "${dir}/Deploy/ships-${version}-webgl.zip" . && cd ${dir}
fi 
